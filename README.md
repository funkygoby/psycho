# Psycho

Application graphique du test de biais cognitif.
Testé sur Windows7+, Linux, OpenBSD.

# Utilisation

Les 3 catégories (2 attributs, 1 concept), ainsi que le corpus de synonymes, sont à renseigner dans le fichier "synonyms.csv". La 1ère ligne concerne les 3 catégories, le reste constitue le corpus de synonyme, précédé de leur catégorie propre (en partant de 0).

Voici un fichier exemple :

bon;mauvais;animal
0;gentil
0;doux
0;amical
0;sociable
0;juste
1;colère
1;méchant
1;blessant
1;rage
1;violent
2;chien
2;chat
2;vache
2;requin
2;pelican

Executer psycho.py
À la fin du test, les résultats se trouvent dans un fichier daté dans le dossier results.

# Description du test

La base du test consiste à classer des mots dans 3 catégories (bon, mauvais, animal) différentes via les touches 'E' et 'I' du clavier.
Le temps de réponse est chronométré et les résultats sont enregistrés dans un fichier .csv (importable ds un tableur).
Chaque catégorie est accompagnée d'un corpus de 5 mots


Plus tard, la 3ème catégorie, le concept ("animal") est présentée, collé à l'attribut1, à gauche, puis à l'attribut2, à droite.

## Déroulé

D'abord, une présentation est affiché puis le test démarre.
Le test est constitué de 3 blocs. Chaque bloc est précédé d'une page informative.

Les 3 blocs :

0- entrainement : 2 catégories sont représentées, l'une à gauche ("bon"), l'autre à droite ("mauvais"). Ce sont les attributs. Les synonymes sont présentés au centre de l'écran 2 fois chacuns, l'ordre est aléatoire (en tout 2*2*5 synonymes seront affichés)
1- bloc1 : on ajoute un concept "animal" en plus, collé (couplé) à l'un des attributs et on refait le test avec des synonymes associés au concept ajouté. Les mots sont présentés 2 fois pour le concept et l'attribut 2*2*5; pour l'attribut seul, les mots sont présentés 3 fois pour équilibrer 3*5
2- bloc 2: identique au bloc 1 sauf que le concept change de coté. La répétition des mots est changée.

On chronomètre le temps de réponse.
Si réponse fausse, on l'indique et on attend que la personne réponde correctement.

Dans les résultats, on veut avoir la catégorie du mot, la réponse du mec et son chrono.

# Implémentation

Au départ, le programme lit le fichier synonyms.csv. Pour tricher, la première ligne contient les 2 attributs et le concept.
Le reste contient les synonymes à classer accompagné de la bonne réponse.

On crée une nouvelle liste qui contient le bon dosage pour chacune des catégories en fonction du bloc concerné. Puis on mélange la liste.

Chaque réponse est chronométrée.
Si réponse fausse, un flag est levé, un -1 est enregistré comme réponse et on discard les inputs (gràce à ce flag) jusqu'à ce que l'user ait mis un truc bon. Ça relance la suite du test.

Entre chaque bloc, une ligne blanche est insérée dans les résultat pour la lisibilité.
Puis une information est affichée à l'écran.

À la fin du dernier test du dernier block, les résultats sont stockés dans un fichier daté.
Les résultats sont présentés comme ça (dans le terminal et le csv): D'abord le mot demandé (sa catégorie et le mot lui même), puis la réponse et le chrono. -1 réponse fausse donnée, tout le reste veut dire bonne réponse. Bien sûr dans le bloc une bonne réponse pour catégorie 2 sera 1 (droit), dans le bloc 2, ça sera 0 (gauche)

2;dog;1;0.5
1;nasty;-1;0.3

L'action de la souris est volontairement désactivé (les oncommand pas renseignés).
Les binding keyboard (e=gauche, i=droite) sont volontairement initialisé juste après avoir affiché un test. Ils sont immédiatement désactivés après une seule utilisation. Sans ça, vu que les events sont stackés, si l'utilisateur appuie 3 fois sur 'e', les 3 prochains tests seront répondus "gauche". Il n'y pas moyen de vider la queue des events, il faut breaker dès qu'on veut ignorer les events keyboard.
