#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tkinter
import random
import time
import csv
import os

lang = 'fr'
catSize = 5 # number of term in each cat

# Every strings are stored here for readability
class Text:
	def __init__ (self, attrs, synonyms):

		self.content = ''
		i = 0
		for attr in attrs:
			syns = synonyms[i][1]
			for syn in synonyms[i+1:i+5]:
				syns += ', '+syn[1]
			self.content += attr+' : '+syns+'\n'
			i += catSize
	
		if lang == 'fr':
			self.blockm1 = (
				"Intro\n\n"
			)
			self.block0 = (
				"Training Block\n\n"
				"Pour cette expérimentation vous allez appuyer sur la touche 'E'(réponse gauche) ou 'I' (réponse droite) pour ranger des mots dans des groupes aussi vite que possible.\n\n"
				"Voici les catégories et les mots qui leur appartiennent :\n"
			)
			self.block1 = (
				"Block 1\n\n"
				"Appuyez sur 'E' pour stable.\n\n"
				"Appuyez sur 'I' pour modifiable et '"+attrs[2]+"'.\n\n"
				"Chaque item n'appartient qu'à une seule catégorie.\n\n"
				"Si vous faites une erreur une croix rouge apparaîtra.\n"
				"Appuyez sur l'autre touche de réponse pour continuer.\n\n"
			)
			self.block2 = (
				"Block 2\n\n"
				"Now the concept and related words belong to the left side.\n\n"
			)
			self.closing = "Appuyez sur 'E' ou 'I' quand vous êtes prêt"

			# Static text
			self.oneliner = "Appuyez sur 'E' pour réponse gauche, 'I' pour réponse droite"
		else: # en
			self.blockm1 = (
				"Intro\n\n"
			)
			self.block0 = (
				"Training Block\n\n"
				"Associate each word to its corresponding category.\n"
			)
			self.block1 = (
				"Block 1\n\n"
				"Same as before but there is a new concept and words ('"+attrs[2]+"') that belong to the right side.\n\n"
			)
			self.block2 = (
				"Block 2\n\n"
				"Now the concept and related words belong to the left side.\n\n"
			)
			self.closing = "Press 'E' or 'I' when ready"

			# Static Text
			self.oneliner = "Press 'E' for left answer, 'I' for right answer"

	def getBlockm1(self):
		return self.blockm1

	def getBlock0(self):
		return self.block0+self.content+'\n'+self.closing

	def getBlock1(self):
		return self.block1+self.closing
	
	def getBlock2(self):
		return self.block2+self.closing

class Psycho(tkinter.Frame):
	def __init__(self, master):
		super().__init__(master)
		self.master = master # We need root.destroy() later
		self.master.protocol('WM_DELETE_WINDOW', self.onQuit) # Deals with the close event

		# Constants
		self.sleepTest = 0.3 # time between each test
		bg = 'black'
		self.fg = 'green'
		self.fgAlt = 'white'
		font = 'helvetica 18 bold'

		# Variables backend
		self.currentBlock = -2 # -1 for generals info, 0 for training, 1 for block1, 2 for 2
		self.currentTest = -1
		self.isInfo = False # Is set when we are in an info message so that the next onKey can proceed to the 1st test of the block
		self.isError = False # Is set when wrong answer and is lifted when right finally given so we can discard this answer
		self.timeStamp = time.time() # We need to time every answer
		self.attrs = [] # Only 3 elmts: 2 attribues + the concept
		self.synonyms = [] # All the synonyms corpus
		self.tests = [] # The synonyms in random order and redundancy depending on the current block
		self.results = []

		# Build the GUI
		self.configure(bg=bg)
		self.pack(fill=tkinter.BOTH, expand=True)
		# We need a grid because widget will be forget and then need to be re-grid to the right position
		self.rowconfigure(0) # attributes
		self.rowconfigure(1) # concept
		self.rowconfigure(2, weight=1) # synonym
		self.rowconfigure(3) # error widget
		self.rowconfigure(4) # one-liner message
		self.columnconfigure(0, weight=1)
		self.columnconfigure(1, weight=1)

		# Attributes, Concept and the little one-liner at the bottom
		left = tkinter.Label(self, font=font, bg=bg, fg=self.fg)
		left.grid(row=0, column=0, padx=10, sticky=tkinter.W)
		right = tkinter.Label(self, font=font, bg=bg, fg=self.fg)
		right.grid(row=0, column=1, padx=10, sticky=tkinter.E)
		oneliner = tkinter.Label(self, bg=bg, fg=self.fgAlt)
		oneliner.grid(row=4, columnspan=2)

		# Synonym and error
		# The synonyms need to change his text, so we act on text=
		# The concept doesn't change his text but is position. We forget() it then grid() it
		# The error doesn't need to move so we use remove instead because it keeps the property (position) and we can use grid() without arg to bring it back at the same place. In fact, to avoid row resize, we will just empty the text and fill it back ....
		self.concept = tkinter.Label(self, font=font, bg=bg, fg=self.fgAlt)
		self.synonym = tkinter.Label(self, font=font, bg=bg, fg=self.fg, wraplength=600)
		self.synonym.grid(row=2, columnspan=2)
		self.error = tkinter.Label(self, font='helvetica 42 bold', bg=bg, fg='red')
		self.error.grid(row=3, columnspan=2)

		self.loadTests()
		# Check that we have 3*5 words. Remove this
		if not len(self.synonyms) == 3*catSize:
			print("Wrong number of synonyms")

		# Initialise strings. One can also use stringVar
		left.configure(text=self.attrs[0]) # or left['text'] = self.attrs[0]
		right.configure(text=self.attrs[1])
		self.concept.configure(text='ou\n'+self.attrs[2])
		self.text = Text(self.attrs, self.synonyms)
		oneliner.configure(text=self.text.oneliner)

		# set focus so that keyboard is catched
		self.focus_set()

		# self.currentBlock=0 # for jumping in a specific block
		self.nextBlock()

	def loadTests(self):
		with open('synonyms.csv', newline='') as  csvfile:
			synonyms = csv.reader(csvfile, delimiter=';') # delimiter=',', quotechar='"'
			# Load the first line on the file that defines or attributes and concept
			self.attrs = next(synonyms)
			# Read the whole test/synonym
			for syn in synonyms:
				self.synonyms.append([int(syn[0]),syn[1]])
	
	def saveResults(self):
		os.makedirs('results', exist_ok=True)
		with open('results/results-'+time.strftime('%Y%m%d-%H%M%S')+'.csv', 'w', newline='') as csvfile:
			results = csv.writer(csvfile, delimiter=';')
			for result in self.results:
				results.writerow(result)
				
	def nextTest(self):
		self.currentTest += 1
		i = self.currentTest

		# If tests of currentBlock are done, exit
		if i == len(self.tests):
			self.results.append([])
			self.nextBlock()
			return

		# Intertest sleep
		self.synonym.configure(text='')
		self.synonym.update()
		time.sleep(self.sleepTest)

		# New test begins
		if self.tests[i][0] == 2:
			self.synonym.configure(fg=self.fgAlt)
		else:
			self.synonym.configure(fg=self.fg)

		self.synonym.configure(text=self.tests[i][1])
		# self.master.update_idletasks() # Update all pending events (ex: those that redraw)
		self.master.update() # Process ALL events (more aggressive than _idletasks()). Here, it is used to flush the keyboard events
		self.bindKeys()
		self.timeStamp = time.time()

	def nextBlock(self):
		self.currentBlock += 1

		if self.currentBlock == -1: # General Infos
			self.tests = []
			self.synonym.configure(text=self.text.getBlockm1())
		elif self.currentBlock == 0:
			self.tests = []
			self.tests.extend(2*self.synonyms[0:5])
			self.tests.extend(2*self.synonyms[5:10])
			random.shuffle(self.tests)
			self.synonym.configure(text=self.text.getBlock0())
		elif self.currentBlock == 1:
			self.tests = []
			self.tests.extend(3*self.synonyms[0:5])
			self.tests.extend(2*self.synonyms[5:10])
			self.tests.extend(2*self.synonyms[10:15])
			random.shuffle(self.tests)
			self.concept.grid(row=1, column=1, padx=10, sticky=tkinter.E)
			self.concept.configure(justify=tkinter.RIGHT)
			self.synonym.configure(text=self.text.getBlock1())
		elif self.currentBlock == 2:
			self.tests = []
			self.tests.extend(2*self.synonyms[0:5])
			self.tests.extend(3*self.synonyms[5:10])
			self.tests.extend(2*self.synonyms[10:15])
			random.shuffle(self.tests)
			self.concept.grid(row=1, column=0, padx=10, sticky=tkinter.W)
			self.concept.configure(justify=tkinter.LEFT)
			self.synonym.configure(text=self.text.getBlock2())
		else:
			self.onQuit()
			return

		print("Block:", self.currentBlock)
		self.master.update()
		self.currentTest = -1
		self.isInfo = True
		self.bindKeys()

	# Keyboards events
	def bindKeys(self):
		self.bind('<e>', self.onKey);
		self.bind('<i>', self.onKey);

	def unbindKeys(self):
		self.bind('<e>', lambda e: "break")
		self.bind('<i>', lambda e: "break")

	def checkAnswer(self, side):
		if self.currentBlock == 0: #training
			if side == self.tests[self.currentTest][0]:
				return True
			else:
				return False
		elif self.currentBlock == 1:
			if side == self.tests[self.currentTest][0]:
				return True
			elif self.tests[self.currentTest][0] == 2 and side == 1 : # here right side is right answer if concept is proposed
				return True
			else:
				return False
		elif self.currentBlock == 2:
			if side == self.tests[self.currentTest][0]:
				return True
			elif self.tests[self.currentTest][0] == 2 and side == 0: # here left side is right answer if concept is proposed
				return True
			else:
				return False

	def onError(self, d):
		if not self.isError: # Only if the flag is not yet set
			self.isError = True
			self.error.configure(text='X')
			print(self.tests[self.currentTest], 'error', d)
			self.results.append([self.tests[self.currentTest][0],self.tests[self.currentTest][1],0,d])
		self.bindKeys()

	def onKey(self,event):
		self.unbindKeys() # Unbind keyboard so every "accidental" events that happen before the next bind() will be flushed.

		if self.isInfo: # We were in an info message, so we can start the new block
			self.isInfo = False
			self.nextTest()
			return

		d = time.time() - self.timeStamp
		if event.keysym == 'e':
			if self.checkAnswer(0):
				if not self.isError: # Normal behaviour
					print(self.tests[self.currentTest], 'left', d)
					self.results.append([self.tests[self.currentTest][0],self.tests[self.currentTest][1],1,d])
				else:
					self.isError = False
					self.error.configure(text='')
			else:
				self.onError(d)
				return
		elif event.keysym == 'i':
			if self.checkAnswer(1):
				if not self.isError:
					print(self.tests[self.currentTest], 'right', d)
					self.results.append([self.tests[self.currentTest][0],self.tests[self.currentTest][1],1,d])
				else:
					self.isError = False
					self.error.configure(text='')
			else:
				self.onError(d)
				return

		self.nextTest()

	def onQuit(self):
		self.saveResults()
		self.master.destroy() # Kills background operations too (loops)

if __name__ == "__main__":
	root = tkinter.Tk()
	root.title("Psycho")
	root.geometry("800x600")
	Psycho(root)
	root.mainloop()
